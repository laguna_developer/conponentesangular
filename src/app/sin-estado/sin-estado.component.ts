import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Producto } from './../interfaces/producto';

@Component({
  selector: 'app-sin-estado',
  templateUrl: './sin-estado.component.html',
  styleUrls: ['./sin-estado.component.css']
})
export class SinEstadoComponent implements OnInit {

  @Input() producto: Producto;
  @Output() productoSeleccionado: EventEmitter<Producto> = new EventEmitter();
  private disabled: boolean;
  public accionCompra: string;

  constructor() {
  }

  ngOnInit(): void {
    this.accionCompra = `BUY FOR $${this.producto.precio}`;
  }

  seleccionarItem(): void {
    this.disabled = true;
    this.accionCompra = 'ADD TO CART';
    this.productoSeleccionado.emit(this.producto);
  }

  isItemDisabled(): boolean {
    console.log(this.producto.titulo);
    return !!this.disabled;
  }

}
