export class Tienda {

  tiendaItems: Array<object>;

  constructor() {

    this.tiendaItems = [
      {
        texto: 'H+ Sport is dedicated to creating eco-friendly, high-quality, nutrient-rich, nutritional products that enhance active lifestyles.',
        titulo: 'Orange Mineral Water',
        imagen: 'mineralwater-orange.jpg',
        precio: 20
      },
      {
        texto: 'H+ Sport is dedicated to creating eco-friendly, high-quality, nutrient-rich, nutritional products that enhance active lifestyles.',
        titulo: 'Strawberry Mineral Water',
        imagen: 'mineralwater-strawberry.jpg',
        precio: 33
      },
      {
        texto: 'H+ Sport is dedicated to creating eco-friendly, high-quality, nutrient-rich, nutritional products that enhance active lifestyles.',
        titulo: 'Chocolate Protein Bar',
        imagen: 'proteinbar-chocolate.jpg',
        precio: 120
      },
      {
        texto: 'Agua, Azucar, Malta de cebada 3, 8%, dioxido de carbono, Colorante: E150c, Acidulante: Acido Citrico E330, Aroma, Lupolo, Vitaminas.',
        titulo: 'Pony Malta Bavaria 330 ml',
        imagen: 'Pony-Malta-Bavaria.jpg',
        precio: 1200
      }
    ];
  }
}
