import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alerta-confirmacion',
  templateUrl: './alerta-confirmacion.component.html',
  styleUrls: ['./alerta-confirmacion.component.css']
})
export class AlertaConfirmacionComponent implements OnInit {
  public active: boolean;

  constructor() {
    this.active = false;
  }

  ngOnInit(): void {
  }

  ocultar(): void {
    this.active = false;
  }

  mostrar(): void {
    this.active = true;
  }
}
